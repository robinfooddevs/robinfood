<?php
include 'includes/dbh-inc.php';
if (isset($_REQUEST['admin_action']) && !empty($_REQUEST['admin_action'])) {
    if ($_REQUEST['admin_action'] == 'sendReadyNotification' && !empty($_REQUEST['order_id'])) {
        $order_id = $_REQUEST['order_id'];
        $notification_type = 3; //order is ready
        $created = date("Y-m-d H:i:s", time());
        $stmt = $conn->prepare("INSERT INTO notifications (order_id, type, created) VALUES (?, ?, ?)");
        $stmt->bind_param("iis", $order_id, $notification_type, $created);
        $result = $stmt->execute();
        $stmt->close();

        echo $result? 'ok' : 'err';die;
    }
}