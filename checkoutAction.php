<?php session_start();

include 'includes/dbh-inc.php';

function debug_to_console($data) {
    if (is_array($data) || is_object($data)) {
        echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
    } else {
        echo("<script>console.log('PHP: ".$data."');</script>");
    }
}

function debug_to_alert($data) {
    if (is_array($data) || is_object($data)) {
        echo("<script>window.alert('PHP: ".json_encode($data)."');</script>");
    } else {
        echo("<script>window.alert('PHP: ".$data."');</script>");
    }
}

if(isset($_REQUEST['checkout_action']) && !empty($_REQUEST['checkout_action'])) {
    if($_REQUEST['checkout_action'] == 'getStoredUserAddress') {
//        debug_to_alert("Isset sessCustomerID: " . isset($_SESSION['sessCustomerID']));
        if (isset($_SESSION['sessCustomerID'])) {
            $result = $conn->query("SELECT user_address FROM users WHERE user_id = {$_SESSION['sessCustomerID']}");
//            debug_to_alert("Num rows: " . $result -> num_rows);
            if ($result -> num_rows > 0) {
                $row = $result->fetch_assoc();
                $data['status'] = 'ok';
                $data['address'] = $row['user_address'];
            } else {
                $data['status'] = 'err';
                $data['address'] = '';
            }
        } else {
            $data['status'] = 'err';
            $data['address'] = '';
        }
        echo json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        unset($data);
    }
}