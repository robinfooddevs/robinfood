
var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_2: 'short_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    var options = {
        types: ['geocode'],
        componentRestrictions: {country: 'it'}
    };
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('route')), options);

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    var geocoder = new google.maps.Geocoder;
    document.getElementById('locbtn').addEventListener('click', function() {
        geocodeCurrLatLong(geocoder);
    });
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    // <!-- for (var component in componentForm) { -->
    // <!-- document.getElementById(component).value = ''; -->
    // <!-- document.getElementById(component).disabled = false; -->
    // <!-- } -->

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    if (typeof place.address_components !== "undefined") {
        fillInFields(place.address_components);
    }
    document.getElementById('route').blur();
}

function fillInFields(address_components) {
    for (var i = 0; i < address_components.length; i++) {
        var addressType = address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = address_components[i][componentForm[addressType]];
            if (addressType === 'administrative_area_level_2') {
                if (!checkIfProvinceIsServed(val)) { // province does not exist
                    window.alert('This province is not served.');
                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                    }
                    return;
                }
            }
            document.getElementById(addressType).value = val;
        }
    }
}

function checkIfProvinceIsServed(val) {
    for (var i = 0; i < document.getElementById('administrative_area_level_2').length; i++) {
        if (document.getElementById('administrative_area_level_2').options[i].value === val) {
            return true;
        }
    }
    return false;
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function geocodeCurrLatLong(geocoder) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(position) { //on success
                var latlng = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            console.log(results[0]);
                            fillInFields(results[0].address_components);
                        } else {
                            window.alert('No results found.');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            });
    }
}