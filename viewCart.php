<?php
// initialize shopping cart class
include 'Cart.php';
$cart = new Cart;

$fmt = new NumberFormatter('it_IT', NumberFormatter::CURRENCY);
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <title>View Cart - RobinFood</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://fonts.googleapis.com/css?family=Quicksand:500" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
    body {
      font-weight: bold;
      background: #f8f9fa;
    }
    .cart-container:after {
        content : "";
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        background-image: url(images/vino.jpg);
        background-position: center;
        background-size: cover;
        background-attachment: fixed;
        width: 100%;
        height: 100vh;
        opacity : 0.3;
        z-index: -1;
    }
    .container{padding: 50px;}
    h1 {
      text-align: center;
      margin-bottom: 40px;
      font-family: Quicksand;
      font-style: oblique;
      text-transform: uppercase;
      font-size: 40px;
      font-weight: bold;
    }
    @media screen and (min-width: 767px) {
      input[type="number"]{width: 20%;}
      .return-menu {
        width: 350px;
        float: left;
        margin-left: 100px;
      }
      .checkout {
        width: 350px;
        float: right;
        margin-right: 100px;
      }
    }
    </style>
    <script>
    function updateCartItem(obj,id){
        if(obj.value < 0) {
          alert('Non puoi inserire un valore negativo come quantità!');
          obj.value = 1;
          return;
        }
        $.get("cartAction.php", {action:"updateCartItem", id:id, qty:obj.value}, function(data){
            if(data == 'ok'){
                location.reload();
            }else{
                alert('Cart update failed, please try again.');
            }
        });
    }
    </script>
</head>
</head>
<body>
  <div class="cart-container">
    <h1>Il mio carrello</h1>
    <table class="table">
    <thead>
        <tr>
            <th scope="col">Prodotto</th>
            <th scope="col">Prezzo</th>
            <th scope="col">Quantità</th>
            <th scope="col">Subtotale</th>
            <th scope="col">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if($cart->total_items() > 0){
            //get cart items from session
            $cartItems = $cart->contents();
            foreach($cartItems as $item){
        ?>
        <tr>
            <td><?php echo $item["name"]; ?></td>
            <td><?php echo $fmt->formatCurrency($item["price"], "EUR"); ?></td>
            <td><input type="number" min="0" title="Quantità" class="form-control text-center" value="<?php echo $item["qty"]; ?>" onchange="updateCartItem(this, '<?php echo $item["rowid"]; ?>')"></td>
            <td><?php echo $fmt->formatCurrency($item["subtotal"], "EUR"); ?></td>
            <td>
                <!--<a href="cartAction.php?action=updateCartItem&id=" class="btn btn-info"><i class="glyphicon glyphicon-refresh"></i></a>-->
                <a href="cartAction.php?action=removeCartItem&id=<?php echo $item["rowid"]; ?>" class="btn btn-danger" onclick="return confirm('Sei sicuro di non voler più il prodotto?')"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
        <?php } }else{ ?>
        <tr><td colspan="2"><p>Il tuo carrello è vuoto.....</p></td>
        <?php } ?>
    </tbody>
    <tfoot>
      <td colspan="3"></td>
      <td class="text-center" colspan="2"><strong>Totale <?php echo $fmt->formatCurrency($cart->total(), "EUR"); ?></strong></td>
    </tfoot>
    </table>
      <?php if($cart->total_items() > 0): ?>
    <div class="checkout" style="margin-bottom:10px;">
      <a href="checkout_new.php" class="btn btn-success btn-block" style="font-weight:bold;color:#000;padding-top:10px;padding-bottom:10px;">Procedi al checkout <i class="glyphicon glyphicon-menu-right"></i></a>
    </div>
      <?php endif; ?>
    <div class="return-menu">
      <a href="homepage.php" class="btn btn-warning btn-block" style="font-weight:bold;color:#000;padding-top:10px;padding-bottom:10px;"><i class="glyphicon glyphicon-menu-left"></i> Continua lo shopping</a>
    </div>
  </div>
</body>
</html>
