<?php
// initialize shopping cart class
include 'Cart.php';
$cart = new Cart;

// include database configuration file
include 'includes/dbh-inc.php';
if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
    if($_REQUEST['action'] == 'addToCart' && !empty($_REQUEST['id'])){
        $productID = $_REQUEST['id'];
        // get product details
        $stmt = $conn->prepare("SELECT * FROM products WHERE product_id = ?");
        $stmt->bind_param("i", $productID);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $stmt->close();
//        $query = $conn->query("SELECT * FROM products WHERE product_id = ".$productID);
//        $row = $query->fetch_assoc();
        $itemData = array(
            'id' => $row['product_id'],
            'name' => $row['name'],
            'price' => $row['price'],
            'qty' => 1
        );
        
        $insertItem = $cart->insert($itemData);
        $redirectLoc = $insertItem?'viewCart.php':'homepage.php';
        header("Location: ".$redirectLoc);
    }elseif($_REQUEST['action'] == 'updateCartItem' && !empty($_REQUEST['id'])){
        $itemData = array(
            'rowid' => $_REQUEST['id'],
            'qty' => $_REQUEST['qty']
        );
        $updateItem = $cart->update($itemData);
        echo $updateItem?'ok':'err';die;
    }elseif($_REQUEST['action'] == 'removeCartItem' && !empty($_REQUEST['id'])){
        $deleteItem = $cart->remove($_REQUEST['id']);
        header("Location: viewCart.php");
    }elseif($_REQUEST['action'] == 'placeOrder' && $cart->total_items() > 0 && !empty($_SESSION['sessCustomerID'])){
        // insert order details into database
        $user_id = $_SESSION['sessCustomerID'];
        $total_price = $cart->total();
        $created = date("Y-m-d H:i:s");
        $modified = date("Y-m-d H:i:s");
        $delivery_hour = date('H:i:s', strtotime($_POST['deliveryHour']));
        $address = array(
            'route' => $_POST['route'],
            'street_number' => $_POST['street_number'],
            'locality' => $_POST['locality'],
            'administrative_area_level_2' => $_POST['administrative_area_level_2'],
            'postal_code' => $_POST['postal_code']
        );
        if(isset($_POST['address2']) && !empty($_POST['address2'])) {
            $address['address2'] = $_POST['address2'];
        }
        $encoded_address = json_encode($address, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $stmt = $conn->prepare("INSERT INTO orders (user_id, total_price, created, modified, delivery_hour, address)
                                     VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("idssss", $user_id, $total_price, $created, $modified, $delivery_hour, $encoded_address);
        $insertOrder = $stmt->execute();
        $stmt->close();

//        $insertOrder = $conn->query("INSERT INTO orders (user_id, total_price, created, modified) VALUES ('".$_SESSION['sessCustomerID']."', '".$cart->total()."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
        
        if($insertOrder){
            $orderID = $conn->insert_id;
            $sql = '';
            // get cart items
            $cartItems = $cart->contents();
            //preparing statement
            $stmt_ord = $conn->prepare("INSERT INTO order_items (order_id, product_id, quantity) VALUES (?, ?, ?)");
            $stmt_ord->bind_param("iii", $order_id, $product_id, $quantity);
            foreach($cartItems as $item){
                $order_id = $orderID;
                $product_id = $item['id'];
                $quantity = $item['qty'];
                $insertOrderItems = $stmt_ord->execute();
//                $sql .= "INSERT INTO order_items (order_id, product_id, quantity) VALUES ('".$orderID."', '".$item['id']."', '".$item['qty']."');";
            }
            $stmt_ord->close();

            $notification_type = 2;
            $notify_order_id = $orderID;
            $created_notify = date("Y-m-d H:i:s", time());
            $stmt_notify = $conn->prepare("INSERT INTO notifications (order_id, type, created) VALUES (?, ?, ?)");
            $stmt_notify->bind_param("iis", $notify_order_id, $notification_type, $created_notify);
            $result_stmt_notify = $stmt_notify->execute();
            $stmt_notify->close();
            // insert order items into database
//            $insertOrderItems = $conn->multi_query($sql);

            if (isset($_POST['save-info']) && $_POST['save-info'] == 'save') {
                $stmt_save_info = $conn->prepare("UPDATE users SET user_address = ? WHERE user_id = {$_SESSION['sessCustomerID']}");
                $stmt_save_info->bind_param("s", $encoded_address);
                $insertAddress = $stmt_save_info->execute();
                $stmt_save_info->close();
            }


            if($insertOrderItems){
                $cart->destroy();
                header("Location: orderSuccess.php?id=$orderID");
            }else{
                header("Location: checkout.php");
            }
        }else{
            header("Location: checkout.php");
        }
    }else{
        header("Location: homepage.php");
    }
}else{
    header("Location: homepage.php");
}