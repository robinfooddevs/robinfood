<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);
	include_once 'header.php';

	include_once 'main-content.php';

	include_once 'footer.php';

	if(isset($_GET['login'])) {
	    if ($_GET['login'] == 'error') {
	        echo "<script>alert('Username o password errati!')</script>";
        }
    }

    if(isset($_GET['signup'])) {
        if ($_GET['signup'] == 'success') {
            echo "<script>alert('Registrazione avvenuta con successo!')</script>";
        }
    }