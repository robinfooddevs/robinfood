<?php
    include 'includes/dbh-inc.php';
    $select_orders_not_done = "SELECT *
                               FROM orders
                               WHERE orders.order_id IN (SELECT notifications.order_id
                   		                                 FROM notifications
                   		                                 GROUP BY notifications.order_id
                                                         HAVING MAX(notifications.type) = 2 AND MIN(notifications.type) = 2)
                               ORDER BY orders.delivery_hour ASC";
    $result = $conn->query($select_orders_not_done);
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="orders-view.css">
    <title>Visualizza Ordini</title>
  </head>
  <body class="bg-light">
  <div class="container">
      <div class="pt-5">
          <a class="btn btn-primary" href="homepage.php" role="button"><i class="fa fa-angle-left mr-3"></i>Torna alla home</a>
      </div>
  <div class="py-5">
      <h2 class="text-center">Visualizza Ordini</h2>
  </div>
      <script>
          function orderReady(order_id) {
              $.post("adminAction.php", {admin_action:"sendReadyNotification", order_id:order_id}, function (data) {
                  if (data == 'ok') {
                      alert("Notifica inviata correttamente!");
                      location.reload();
                  } else {
                      alert("Errore nell'invio della notifica.");
                  }
              });
          }
      </script>
<!--      <div class="row">-->
          <?php if ($result->num_rows > 0): ?>
          <div id="accordion" class="accordion">
              <?php
                $curr_row_count = 1;
                while ($row = $result->fetch_assoc()) {
              ?>
              <div class="card ordine">
                  <div class="card-header" id="heading<?=$curr_row_count?>" data-toggle="collapse" data-target="#collapse<?=$curr_row_count?>" aria-expanded="false" aria-controls="collapse<?=$curr_row_count?>">
                      <div class="row">
                          <div class="col">
                      <h5 class="mb-0">
                          <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?=$curr_row_count?>" aria-expanded="false" aria-controls="collapse<?=$curr_row_count?>">
                              Ordine #<?=$row['order_id']?>
                          </button>
                      </h5>
                          </div>

                      <div class="col pl-0 my-auto">
                          <span class="d-inline-block"><strong>Consegna:</strong></span>
                          <span><?php echo date("H:i", strtotime($row['delivery_hour']))?></span>
                      </div>
                          <div class="col">
                          <button type="button" class="btn btn-success btn-pronto" onclick="orderReady(<?=$row['order_id']?>)">Ordine pronto</button>
                          </div>
                      </div>
                  </div>

                  <div id="collapse<?=$curr_row_count?>" class="collapse" aria-labelledby="heading<?=$curr_row_count?>" data-parent="#accordion">
                      <div class="card-body">
                          <div class="row">
                              <?php
                                $select_user_query = "SELECT * FROM users WHERE user_id = {$row['user_id']}";
                                $result_select_user = $conn->query($select_user_query);
                                if ($result_select_user->num_rows > 0) {
                                    $row_user = $result_select_user->fetch_assoc();
                                } else {
                                    die("User not found!");
                                }
                              ?>
                              <div class="col-md-3">
                                  <span class="d-md-block"><strong>Nome: </strong></span>
                                  <span><?php echo "{$row_user['user_nome']} {$row_user['user_cognome']}"?></span>
                              </div>
                              <div class="col-md-4">
                                  <span class="d-md-block"><strong>Indirizzo di consegna: </strong></span>
                                  <span><?php
                                      $address = json_decode($row['address'], true);
                                      echo "{$address['route']} {$address['street_number']} {$address['postal_code']} {$address['locality']} {$address['administrative_area_level_2']}";
                                      ?></span>
                              </div>
                              <div class="col-md-3">
                                  <span class="d-md-block"><strong>Telefono: </strong></span>
                                  <span><?=$row_user['user_phone']?></span>
                              </div>
                              <div class="col-md-2">
                                  <span class="d-md-block"><strong>Ora di consegna:</strong></span>
                                  <span><?php echo date("H:i", strtotime($row['delivery_hour']))?></span>
                              </div>
                          </div>
                          <hr>
                          <div class="container">
                          <h4>Articoli</h4>
                          <table class="table table-striped">
                              <thead>
                              <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">Prodotto</th>
                                  <th scope="col">Quantità</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php
                              $select_items_query = "SELECT * FROM order_items WHERE order_id = {$row['order_id']}";
                              $result_items = $conn->query($select_items_query);
                              if ($result_items->num_rows > 0) {
                                  $row_item_no = 1;
                                  while ($row_items = $result_items->fetch_assoc()) {
                              ?>
                              <tr>
                                  <th scope="row"><?=$row_item_no?></th>
                                  <?php
                                    $select_product_query = "SELECT * FROM products WHERE product_id = {$row_items['product_id']}";
                                    $result_product_query = $conn->query($select_product_query);
                                    if ($result_product_query->num_rows > 0) {
                                        $row_product = $result_product_query->fetch_assoc();
                                    } else {
                                        die("Problem in retrieving product!");
                                    }
                                  ?>
                                  <td><?=$row_product['name']?></td>
                                  <td><?=$row_items['quantity']?></td>
                              </tr>
                              <?php
                                  $row_item_no++;
                                  } } else {
                                  echo "Problem in retrieving order items!";
                              } ?>
                              </tbody>
                          </table>
                          </div>
                      </div>
                  </div>
              </div>
              <?php $curr_row_count++;} ?>
          </div>
      <?php else: ?>
      <p>Non ci sono ordini...</p>
      <?php endif; ?>
<!--      </div>-->
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <script>
          $(document).ready(function () {
             $(".btn-pronto").on('click', function (e) {
                 e.stopPropagation();
             });

              // $("#accordion").bind('shown', function() {
              //     var active=$("#accordion2.show").attr('id');
              //     sessionStorage.setItem('activeAccordionGroup', active);
              // });
              $(window).on('beforeunload', function () {
                  if (typeof(Storage) !== "undefined") {
                      sessionStorage.setItem("accordionShownValue", $(".collapse.show").attr('id'));
                  }
              });
             var lastShown = sessionStorage.getItem("accordionShownValue");
             if (lastShown !== "undefined"){
                 $("#" + lastShown).collapse("show");
             }

             setInterval(function () {
                 location.reload();
             }, 30000);
          });
      </script>
  </body>
</html>