-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 18, 2018 alle 17:32
-- Versione del server: 10.1.30-MariaDB
-- Versione PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `robinfood`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `categories`
--

INSERT INTO `categories` (`category_id`, `name`) VALUES
(5, 'bevande'),
(4, 'dolci'),
(3, 'panini'),
(1, 'pizze'),
(2, 'primi');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `delivery_hour` time NOT NULL,
  `address` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `order_items`
--

CREATE TABLE `order_items` (
  `item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struttura della tabella `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `products`
--

INSERT INTO `products` (`product_id`, `name`, `description`, `category_id`, `price`, `image`) VALUES
(9, 'Gnocchi al pomodoro', 'Lorem ipsum ciccio pasticcio', 2, '5.00', 'gnocchi.jpg'),
(10, 'Lasagne alla bolognese', 'Lorem ipsum ciccio pasticcio', 2, '5.00', 'lasagne.jpg'),
(11, 'Cappelletti al ragù', 'Lorem ipsum ciccio pasticcio', 2, '5.50', 'cappelletti.jpg'),
(12, 'Strozzapreti panna e speck', 'Lorem ipsum ciccio pasticcio', 2, '5.50', 'strozzapreti.jpg'),
(13, 'Tortelli burro e salvia', 'Lorem ipsum ciccio pasticcio', 2, '6.00', 'tortelli.jpg'),
(14, 'Tagliatelle ai funghi porcini', 'Lorem ipsum ciccio pasticcio', 2, '6.50', 'tagliatelle_porcini.jpg'),
(17, 'Big Toast', 'Lorem ipsum ciccio pasticcio', 3, '3.00', 'toast.jpg'),
(18, 'Hot dog', 'Lorem ipsum ciccio pasticcio', 3, '3.50', 'hot_dog.jpg'),
(19, 'Cheese dog', 'Lorem ipsum ciccio pasticcio', 3, '4.00', 'cheese_dog.jpg'),
(20, 'Hamburger', 'Lorem ipsum ciccio pasticcio', 3, '5.00', 'hamburger.jpg'),
(21, 'Chickenburger', 'Lorem ipsum ciccio pasticcio', 3, '5.50', 'chickenburger.jpg'),
(22, 'Cheeseburger', 'Lorem ipsum ciccio pasticcio', 3, '5.50', 'cheeseburger.jpg'),
(24, 'Mascarpone', 'Lorem ipsum ciccio pasticcio', 4, '3.50', 'mascarpone.jpg'),
(26, 'Torta della nonna', 'Lorem ipsum ciccio pasticcio', 4, '3.50', 'torta_della_nonna.jpg'),
(27, 'Tartufo nero', 'Lorem ipsum ciccio pasticcio', 4, '3.50', 'tartufo_nero.jpg'),
(28, 'Tartufo bianco', 'Lorem ipsum ciccio pasticcio', 4, '3.50', 'tartufo_bianco.jpg'),
(29, 'Cheesecake', 'Lorem ipsum ciccio pasticcio', 4, '3.50', 'cheesecake.jpg'),
(30, 'Semifreddo al torroncino', 'Lorem ipsum ciccio pasticcio', 4, '3.50', 'semifreddo_torroncino.jpg'),
(38, 'Marinara', 'Lorem ipsum ciccio pasticcio', 1, '3.50', 'pizza_marinara.jpg'),
(39, 'Margherita', 'Lorem ipsum ciccio pasticcio', 1, '4.00', 'pizza_margherita.jpg'),
(41, 'Capricciosa', 'Lorem ipsum ciccio pasticcio', 1, '5.50', 'pizza_capricciosa.jpg'),
(42, 'Americana', 'Lorem ipsum ciccio pasticcio', 1, '5.50', 'pizza_americana.jpg'),
(43, 'Diavola', 'Lorem ipsum ciccio pasticcio', 1, '5.50', 'pizza_diavola.jpg'),
(44, 'Quattro formaggi', 'Lorem ipsum ciccio pasticcio', 1, '6.50', '4_formaggi.jpg'),
(46, 'Acqua 1/2 l', NULL, 5, '1.00', NULL),
(47, 'Acqua 1 l', NULL, 5, '1.80', NULL),
(48, 'Coca Cola', NULL, 5, '2.50', NULL),
(49, 'Sprite', NULL, 5, '2.50', NULL),
(50, 'Fanta', NULL, 5, '2.50', NULL),
(51, 'Birra Moretti 33cl', NULL, 5, '2.50', NULL),
(52, 'Birra Moretti 66cl', NULL, 5, '3.50', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_cognome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_uid` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_psw` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `user_address` longtext COLLATE utf8_unicode_ci,
  `user_type` enum('customer','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'customer',
  `last_notification_view` datetime NOT NULL DEFAULT '1970-01-01 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indici per le tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `notifications_ibfk_1` (`order_id`);

--
-- Indici per le tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indici per le tabelle `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indici per le tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `products_ibfk_1` (`category_id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD UNIQUE KEY `user_uid` (`user_uid`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `order_items`
--
ALTER TABLE `order_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT per la tabella `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
