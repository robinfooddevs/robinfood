<?php
	$dbServername = "localhost";
	$dbUsername = "root";
	$dbPassword = "";
	$dbName = "robinfood";

	$conn = new mysqli($dbServername, $dbUsername, $dbPassword, $dbName);

	if ($conn->connect_error) {
	    die("Unable to connect database: " . $conn->connect_error);
    }

    $conn->query("SET NAMES utf8mb4");
