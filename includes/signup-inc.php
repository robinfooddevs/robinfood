<?php

if(isset($_POST['submit'])) {

    include_once 'dbh-inc.php';

    $nome = mysqli_real_escape_string($conn, $_POST['nome']);
    $cognome = mysqli_real_escape_string($conn, $_POST['cognome']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $phone = mysqli_real_escape_string($conn, $_POST['phone']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $psw = mysqli_real_escape_string($conn, $_POST['psw']);

    //check for empty fields
    if(empty($nome) || empty($cognome) || empty($email) || empty($phone) || empty($uid) || empty($psw)) {
//        header("Location: ../signup.php");
        header("Location: ../homepage.php");
        exit();
    } else {
        //check if input characters are valid
        if(!preg_match("/^[a-zA-Z]*$/", $nome) || !preg_match("/^[a-zA-Z]*$/", $cognome)) {
//            header("Location: ../signup.php?signup=name/surname_invalid");
            header("Location: ../homepage.php?signup=name_surname_invalid");
            exit();
            // } elseif (!preg_match("/[^A-Za-z0-9]+/", $psw) || strlen($psw) < 8 || ($_POST('psw') != $_POST('$rptpsw'))) {
            // header("Location: ../signup.php?signup=password_invalid");
            // exit();
        } else {
            //Check if email is valid
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//                header("Location: ../signup.php?signup=email_invalid");
                header("Location: ../homepage.php?signup=email_invalid");
                exit();
            } else {
                $sql = "SELECT * FROM users WHERE user_uid='$uid'";
                $result = mysqli_query($conn, $sql);
                $resultCheck = mysqli_num_rows($result);

                if($resultCheck > 0) {
//                    header("Location: ../signup.php?signup=usertaken");
                    header("Location: ../homepage.php?signup=usertaken");
                    exit();
                } else {
                    //Hashing the password
                    $hashedPsw = password_hash($psw, PASSWORD_DEFAULT);
                    //Insert the user into the database
                    $sql = "INSERT INTO users (user_nome, user_cognome, user_email, user_phone, user_uid, user_psw)
						                VALUES ('$nome', '$cognome', '$email', '$phone', '$uid', '$hashedPsw');";
                    mysqli_query($conn, $sql);
//                    header("Location: ../signup.php?signup=success");
                    header("Location: ../homepage.php?signup=success");
                    exit();
                }
            }
        }
    }
} else {
//    header("Location: ../signup.php?signup=empty");
    header("Location: ../homepage.php?signup=empty");
    exit();
}
