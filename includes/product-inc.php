<?php
include 'dbh-inc.php';

$target_dir = "../images/";
function displayAlert($message) {
    header("Location: ../homepage.php?addProduct=success");
    echo ("<html><head></head><body><script>alert('".$message."')</script></body></html>");
//    echo $message;
}
if(isset($_POST['submit'])) {

  $nome = mysqli_real_escape_string($conn, $_POST['nome']);
  $descrizione = mysqli_real_escape_string($conn, $_POST['descrizione']);
  $categoria = mysqli_real_escape_string($conn, $_POST['categoria']);
  $prezzo = mysqli_real_escape_string($conn, $_POST['prezzo']);

  $file_upload = empty($_FILES["fileToUpload"]);
  if (isset($_FILES["fileToUpload"]) && !empty($_FILES["fileToUpload"]["name"])) {
      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
      $maxSizeFile = 2000000;
      //Check if image file is a actual image or fake image
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if(!getimagesize($_FILES["fileToUpload"]["tmp_name"])) {
    //      echo("File is an image - " . $check["mime"] . ".");
          echo("Il file non è un immagine.");
      }
      elseif (file_exists($target_file)) {
            $uploadOk = 0;
            echo("Mi dispiace, questo file esiste già.");
        }
        elseif ($_FILES["fileToUpload"]["size"] > $maxSizeFile) {
            $uploadOk = 0;
            echo("Mi dispiace, il tuo file è troppo grande. La dimensione massima consentita è 2 MB.");
        }
        elseif($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            $uploadOk = 0;
            echo("Mi dispiace, sono permessi solo estensioni JPG, JPEG, PNG.");
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo("\nMi dispiace, Il tuo file non è stato caricato.");
            // if everything is ok, try to upload file
        } else {
            $img = $_FILES['fileToUpload']['name'];
          }
}
if (isset($_FILES['fileToUpload']) && isset($uploadOk) && $uploadOk) {
        $query_insert = "INSERT INTO products(name, description, category_id, price, image) VALUES ('$nome', '$descrizione', '$categoria', '$prezzo', '$img')";
        $result = $conn->query($query_insert);
        if ($result && move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $notification_type = 6;
            $created = date("Y-m-d H:i:s", time());
            $stmt = $conn->prepare("INSERT INTO notifications (type, created) VALUES (?, ?)");
            $stmt->bind_param("is", $notification_type, $created);
            $stmt->execute();
            $stmt->close();
            echo("Prodotto aggiunto!");
        } else {
            echo("Inserimento fallito!");
        }
      } elseif (isset($_FILES['fileToUpload']) && empty($_FILES['fileToUpload']['name'])) {
        $query_insert = "INSERT INTO products(name, description, category_id, price) VALUES ('$nome', '$descrizione', '$categoria', '$prezzo')";
        $result = $conn->query($query_insert);
        if ($result) {
            $notification_type = 6;
            $created = date("Y-m-d H:i:s", time());
            $stmt = $conn->prepare("INSERT INTO notifications (type, created) VALUES (?, ?)");
            $stmt->bind_param("is", $notification_type, $created);
            $stmt->execute();
            $stmt->close();
            echo("Prodotto aggiunto!");
        } else {
            echo("Inserimento fallito!");
        }
      }
//    header("Location: ../homepage.php?addProduct=success");
//  echo "<meta http-equiv='Refresh' content='1; URL=../homepage.php'>";
    die;
}

/*if(isset($_POST['delete'])) {
  include 'dbh-inc.php';

  $nome = mysqli_real_escape_string($conn, $_POST['nome']);

  $query_delete ="DELETE FROM products WHERE name = '$nome'";
  $result = $conn->query($query_delete);
  if(!$result) {
    die("error in $query_delete".mysqli_error($conn));
  }
}*/

if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
  if($_REQUEST['action'] == 'deleteItem' && !empty($_REQUEST['id'])){
      $id = mysqli_real_escape_string($conn, $_REQUEST['id']);
      $global_result = false;
      $query_select = "SELECT * FROM products WHERE product_id = {$id}";
      $result1 = $conn->query($query_select);
      if ($result1->num_rows > 0) {
          $row = $result1->fetch_assoc();
          if (!is_null($row['image'])) {
            $delete_path = $target_dir . $row['image'];
            $result_unlink = unlink($delete_path);
          }
          if (isset($result_unlink) && $result_unlink || is_null($row['image'])) {
              $query_delete ="DELETE FROM products WHERE product_id = {$id}";
              $result = $conn->query($query_delete);
              if ($result) {
                  $global_result = true;
              }
          }
      }
      if ($global_result) {
          echo 'ok';
      } else {
          echo 'err';
      }
    }
  }
