<?php

session_start();

if (isset($_POST['submit'])) {

    include 'dbh-inc.php';

    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $psw = mysqli_real_escape_string($conn, $_POST['psw']);

    //Check if inputs are empty
    if(empty($uid) || empty($psw)) {
//        echo "<script>alert('Username o password mancanti!');</script>";
        header("Location: ../homepage.php?login=empty");
        exit();
    } else {
        $sql = "SELECT * FROM users WHERE user_uid='$uid'";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        if($resultCheck < 1) {
//          echo "<script>alert('Username o password errati!');</script>";
            header("Location: ../homepage.php?login=error");
            exit();
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                //De-hashing the password
                $hashedPswCheck = password_verify($psw, $row['user_psw']);
                if ($hashedPswCheck == false) {
                    header("Location: ../homepage.php?login=error");
                    exit();
                } elseif ($hashedPswCheck == true) {
                    //Log in the user here
                    $_SESSION['u_id'] = $row['user_id'];
                    $_SESSION['u_nome'] = $row['user_nome'];
                    $_SESSION['u_cognome'] = $row['user_cognome'];
                    $_SESSION['u_email'] = $row['user_email'];
                    $_SESSION['u_uid'] = $row['user_uid'];
                    $_SESSION['u_type'] = $row['user_type'];
                    header("Location: ../homepage.php?login=success");
                    exit();
                }
            }
        }
    }
} else {
    header("Location: ../homepage.php?login=error");
    exit();
}

?>
