<?php session_start();

include "includes/dbh-inc.php";

/* DEFINITION OF NOTIFICATION TYPES

    ==== CUSTOMER NOTIFICATIONS ====
    1 = Order placed.
    2 = Order received.
    3 = Order is ready.
    4 = Order sent.
    6 = New product added.

    ==== ADMIN NOTIFICATIONS ====
    5 = New order received by admin.

*/

function getNotificationMessage($notification) {
    switch ($notification['type']) {
        case 1:
            return "L'ordine #{$notification['order_id']} è stato inviato correttamente";
            break;
        case 2:
            if ($_SESSION['u_type'] == "customer") {
              return "Il tuo ordine <br/>(#{$notification['order_id']}) è stato ricevuto.";
            } else {
              return "Ricevuto nuovo ordine (#{$notification['order_id']}).";
            }
            break;
        case 3:
            return "L'ordine #{$notification['order_id']} è pronto.";
            break;
        case 4:
            return "L'ordine #{$notification['order_id']} è stato spedito.";
            break;
        case 5:
            return "L'ordine #{$notification['order_id']} è stato ricevuto.";
            break;
        case 6:
            return "Un nuovo prodotto <br/> è stato aggiunto al menù.";
            break;
    }
}
const CUSTOMER_NOTIFICATIONS = [1, 2, 3, 4, 6];
const ADMIN_NOTIFICATIONS = [2, 5];
if(isset($_POST["view"], $_SESSION["u_type"]))
{
//    $field_name = $_SESSION['u_type'] == 'customer' ? 'customer_seen' : 'admin_seen';
//    $output = '';
//    if($_POST["view"] != '')
//    {
//        $query_update_customer_seen = "UPDATE orders SET customer_seen = 1 WHERE user_id = {$_SESSION['u_id']} AND customer_seen = 0";
//        $query_update_admin_seen = "UPDATE orders SET admin_seen = 1 WHERE admin_seen = 0";
////        $update_query = "UPDATE orders SET {$field_name}=1 WHERE user_id = {$_SESSION['u_id']} AND {$field_name}=0";
//        $conn->query($_SESSION['u_type'] == 'customer' ? $query_update_customer_seen : $query_update_admin_seen);
//        $new_count = $conn->affected_rows;
//        $query_select_last_customer = "SELECT * FROM orders WHERE user_id = {$_SESSION['u_id']} ORDER BY order_id DESC LIMIT 5";
//        $query_select_last_admin = "SELECT * FROM orders ORDER BY order_id DESC LIMIT 5";
////    $query = "SELECT * FROM comments ORDER BY comment_id DESC LIMIT 5";
//        $result = $conn->query($_SESSION['u_type'] == 'customer' ? $query_select_last_customer : $query_select_last_admin);
//
//        if($result->num_rows > 0)
//        {
//            $dec_count = 0;
//            while($row = $result->fetch_assoc())
//            {
//                if ($_SESSION['u_type'] == 'customer') {
//                    $message = "Il tuo ordine (#{$row['order_id']}) è pronto";
//                } else {
//                    $message = "Ordine # {$row['order_id']} ricevuto";
//                }
//                $output .= '
//   <li ' . ($dec_count < $new_count && $dec_count < 5 ? "style='background-color: #ADD8E6'" : "") .'>
//    <a href="#">
//     <strong>'.$message.'</strong>
//    </a>
//   </li>
//   <li class="dropdown-divider"></li>
//   ';
//            }
//        }
//        else
//        {
//            $output .= '<li><a href="#" class="text-bold text-italic">No Notification Found</a></li>';
//        }
//    }
//
//
////    $query_1 = "SELECT * FROM comments WHERE comment_status=0";
//    $query_count_customers = "SELECT * FROM orders WHERE user_id = {$_SESSION['u_id']} AND customer_seen = 0";
//    $query_count_admin = "SELECT * FROM orders WHERE user_id = {$_SESSION['u_id']} AND customer_seen = 0";
//    $result_count_customers = $conn->query($_SESSION['u_type'] == 'customer' ? $query_count_customers : $query_count_admin);
//    $count = $result_count_customers->num_rows;
//    $data = array(
//        'notification'   => $output,
//        'unseen_notification' => $count
//    );
//    echo json_encode($data);


    $output = '';
    if ($_POST["view"] != '') { // means that we want to view the notifications (not only update the counter)
        $last_seen_time_query = "SELECT last_notification_view FROM users WHERE user_id = {$_SESSION['u_id']}";
        $result = $conn->query($last_seen_time_query);
        $row = $result->fetch_assoc();
//    if (is_null($row['last_notification_view'])) {
//        $last_notification_view = 0;
//    } else {
        $epoch = strtotime("1970-01-01 00:00:00");
        $last_notification_view =  strtotime($row['last_notification_view']);
//    }
        if ($_SESSION['u_type'] == 'customer') { // request from customer
//            $top5_notifications = "SELECT notifications.* FROM notifications, orders WHERE
//                                  notifications.order_id IS NULL
//                                  OR notifications.order_id = orders.order_id AND orders.user_id = {$_SESSION['u_id']}
//                                   AND notifications.type IN (" .implode(',', CUSTOMER_NOTIFICATIONS).") ORDER BY notifications.notification_id DESC LIMIT 5";
            $top5_notifications = "SELECT * FROM notifications
                                   WHERE order_id IS NULL
                                   OR (order_id IN (SELECT orders.order_id FROM orders WHERE user_id = {$_SESSION['u_id']}))
                                   AND type IN (" .implode(',', CUSTOMER_NOTIFICATIONS).")
                                   ORDER BY notification_id DESC LIMIT 5";

        } else {
            $top5_notifications = "SELECT * FROM notifications WHERE type IN (" .implode(',', ADMIN_NOTIFICATIONS) . ")
                                   ORDER BY notification_id DESC LIMIT 5";
        }
        $result_top5 = $conn->query($top5_notifications);
        if ($result_top5 -> num_rows > 0) {
            while ($row_top5 = $result_top5->fetch_assoc()) {
                $class = strtotime($row_top5['created']) > $last_notification_view ? "new-notification" : "";
                $output .= '
   <li>
    <p class="dropdown-item notification-item ' . $class . '">
     <strong>'.getNotificationMessage($row_top5).'</strong>
    </p>
   </li>
   <li class="dropdown-divider"></li>
   ';
            }
        } else {
            $output .= '<li><p class="dropdown-item notification-item text-bold text-italic">Nessuna notifica...</p></li>';
        }
        $query_set_date = "UPDATE users SET last_notification_view = '" . date("Y-m-d H:i:s", time()) . "' WHERE user_id = {$_SESSION['u_id']}";
//        $conn->query("UPDATE users SET last_notification_view = " . date("Y-m-d H:i:s", time()) . " WHERE user_id = {$_SESSION['u_id']}");
        $result_set = $conn->query($query_set_date);
    }
//    $count_query = "";
    if ($_SESSION['u_type'] == 'customer') {
//        $count_query = "SELECT notifications.* FROM notifications, orders, users WHERE
//                                  users.user_id = {$_SESSION['u_id']}
//                                 AND (notifications.order_id IS NULL
//                                  OR notifications.order_id = orders.order_id AND orders.user_id = users.user_id)
//                                   AND notifications.type IN (" .implode(',', CUSTOMER_NOTIFICATIONS). ")
//                                   AND notifications.created > users.last_notification_view";
        $count_query = "SELECT * FROM notifications
                        WHERE (order_id IS NULL OR order_id IN (SELECT order_id FROM orders WHERE user_id = {$_SESSION['u_id']})
                        AND type IN (" .implode(',', CUSTOMER_NOTIFICATIONS). "))
                        AND created > (SELECT last_notification_view FROM users WHERE user_id = {$_SESSION['u_id']})";
    } else {
//        $count_query = "SELECT notifications.* FROM notifications, users WHERE
//                        users.user_id = {$_SESSION['u_id']}
//                       AND notifications.type IN (" .implode(',', ADMIN_NOTIFICATIONS). ")
//                       AND notifications.created > users.last_notification_view";
        $count_query = "SELECT * FROM notifications
                        WHERE type IN (" .implode(',', ADMIN_NOTIFICATIONS). ")
                        AND created > (SELECT last_notification_view FROM users WHERE user_id = {$_SESSION['u_id']})";
    }

    $result_count = $conn->query($count_query);
    $count = $result_count->num_rows;
    $data = array(
        'notification'   => $output,
        'unseen_notification' => $count
    );
    echo json_encode($data);
}
