<?php
if(!isset($_REQUEST['id'])){
    header("Location: homepage.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>RobinFood - Ordine avvenuto con successo</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
    p{color: #34a853;font-size: 18px;}
    a:hover {
      text-decoration: none;
      color: red;
    }
    @media screen and (min-width: 767px) {
      .alert {
        width: 700px;
        height: 200px;
        top: 50%;
        left: 50%;
        margin-top: 200px;
        margin-left: -350px;
      }
  }
    </style>
</head>
</head>
<body>
    <!-- <h1>Stato Ordine</h1>
    <p>Il tuo ordine è stato inviato correttamente. L' ID del tuo ordine è #<?php echo $_GET['id']; ?></p> -->
  <div class="alert alert-success" role="alert">
    <h4 class="alert-heading text-center">Ben fatto!</h4>
    <p class="text-center">Il tuo ordine è stato inviato correttamente. <br /> L' ID del tuo ordine è #<?php echo $_GET['id']; ?>.</p>
    <hr>
    <a class="text-center" href="homepage.php">Torna all'homepage di RobinFood</a>
  </div>
</body>
</html>
