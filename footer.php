
<footer id="myFooter" style="padding-top:100px;">
	<div class="container" role="8BYQ">
  	<div class="row" style="width:100%;">
    	<div class="col-sm-4">
      	<h2 class="logo"><a href="homepage.php">RobinFood</a></h2>
      </div>
      <div class="col-sm-3">
        <h5>I nostri piatti</h5>
      	<ul>
        	<li><a href="#pizze">Pizze</a></li>
        	<li><a href="#primi">Primi</a></li>
        	<li><a href="#panini">Panini</a></li>
					<li><a href="#dolci">Dolci</a></li>
					<li><a href="#bevande">Bevande</a></li>
      	</ul>
    	</div>
      <div class="col-sm-2">
        <h5>About us</h5>
        <ul>
					<li><a href="#homepage">Home</a></li>
          <li><a href="#about">Chi siamo</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <div class="social-networks">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
        	<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <button type="button" class="btn btn-default">Contattaci</button>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <p>© 2018 RobinFood </p>
  </div>
</footer>

</body>
</html>
