<?php
// include database configuration file
include 'includes/dbh-inc.php';

// initialize shopping cart class
include 'Cart.php';
$cart = new Cart;

$fmt = new NumberFormatter('it_IT', NumberFormatter::CURRENCY);

// redirect to home if cart is empty
if($cart->total_items() <= 0){
    header("Location: homepage.php");
}

// set customer ID in session
$_SESSION['sessCustomerID'] = $_SESSION['u_id'];

// get customer details by session customer ID
$query = $conn->query("SELECT * FROM users WHERE user_id = ".$_SESSION['sessCustomerID']);
$custRow = $query->fetch_assoc();
?>
<!--<!DOCTYPE html>-->
<!--<html lang="en">-->
<!--<head>-->
<!--    <title>Checkout - RobinFood</title>-->
<!--    <meta charset="utf-8">-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<!--    <style>-->
<!--    .container{width: 100%;padding: 50px;}-->
<!--    .table{width: 65%;float: left;}-->
<!--    .shipAddr{width: 30%;float: left;margin-left: 30px;}-->
<!--    .footBtn{width: 95%;float: left;}-->
<!--    .orderBtn {float: right;}-->
<!--    </style>-->
<!--</head>-->
<!--<body>-->
<!--<div class="container">-->
<!--    <h1>Anteprima Ordine</h1>-->
<!--    <table class="table">-->
<!--    <thead>-->
<!--        <tr>-->
<!--            <th>Prodotto</th>-->
<!--            <th>Prezzo</th>-->
<!--            <th>Quantità</th>-->
<!--            <th>Subtotale</th>-->
<!--        </tr>-->
<!--    </thead>-->
<!--    <tbody>-->
<!--        --><?php
//        if($cart->total_items() > 0){
//            //get cart items from session
//            $cartItems = $cart->contents();
//            foreach($cartItems as $item){
//        ?>
<!--        <tr>-->
<!--            <td>--><?php //echo $item["name"]; ?><!--</td>-->
<!--            <td>--><?php //echo $fmt->formatCurrency($item["price"], "EUR"); ?><!--</td>-->
<!--            <td>--><?php //echo $item["qty"]; ?><!--</td>-->
<!--            <td>--><?php //echo $fmt->formatCurrency($item["subtotal"], "EUR"); ?><!--</td>-->
<!--        </tr>-->
<!--        --><?php //} }else{ ?>
<!--        <tr><td colspan="4"><p>No items in your cart......</p></td>-->
<!--        --><?php //} ?>
<!--    </tbody>-->
<!--    <tfoot>-->
<!--        <tr>-->
<!--            <td colspan="3"></td>-->
<!--            --><?php //if($cart->total_items() > 0){ ?>
<!--            <td class="text-center"><strong>Total --><?php //echo $fmt->formatCurrency($cart->total(), "EUR"); ?><!--</strong></td>-->
<!--            --><?php //} ?>
<!--        </tr>-->
<!--    </tfoot>-->
<!--    </table>-->
<!--    <div class="shipAddr">-->
<!--        <h4>Dettagli Consegna</h4>-->
<!--        <p>--><?php //echo "{$custRow['user_nome']} {$custRow['user_cognome']}"; ?><!--</p>-->
<!--        <p>--><?php //echo $custRow['user_email']; ?><!--</p>-->
<!--<!--        <p>--><?php ////echo $custRow['phone']; ?><!--<!--</p>-->
<!--<!--        <p>--><?php ////echo $custRow['address']; ?><!--<!--</p>-->
<!--        <p>My phone number</p>-->
<!--<!--        <p>My address</p>-->
<!---->
<!--        <form>-->
<!--            <div class="form-row">-->
<!--                <div class="form-group col-md-6">-->
<!--                    <label for="inputEmail4">Email</label>-->
<!--                    <input type="email" class="form-control" id="inputEmail4" placeholder="Email">-->
<!--                </div>-->
<!--                <div class="form-group col-md-6">-->
<!--                    <label for="inputPassword4">Password</label>-->
<!--                    <input type="password" class="form-control" id="inputPassword4" placeholder="Password">-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label for="inputAddress">Address</label>-->
<!--                <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <label for="inputAddress2">Address 2</label>-->
<!--                <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">-->
<!--            </div>-->
<!--            <div class="form-row">-->
<!--                <div class="form-group col-md-6">-->
<!--                    <label for="inputCity">City</label>-->
<!--                    <input type="text" class="form-control" id="inputCity">-->
<!--                </div>-->
<!--                <div class="form-group col-md-4">-->
<!--                    <label for="inputState">State</label>-->
<!--                    <select id="inputState" class="form-control">-->
<!--                        <option selected>Choose...</option>-->
<!--                        <option>...</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--                <div class="form-group col-md-2">-->
<!--                    <label for="inputZip">Zip</label>-->
<!--                    <input type="text" class="form-control" id="inputZip">-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="form-group">-->
<!--                <div class="form-check">-->
<!--                    <input class="form-check-input" type="checkbox" id="gridCheck">-->
<!--                    <label class="form-check-label" for="gridCheck">-->
<!--                        Check me out-->
<!--                    </label>-->
<!--                </div>-->
<!--            </div>-->
<!--            <button type="submit" class="btn btn-primary">Sign in</button>-->
<!--        </form>-->
<!--    </div>-->
<!--    <div class="footBtn">-->
<!--        <a href="homepage.php" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Continue Shopping</a>-->
<!--        <a href="cartAction.php?action=placeOrder" class="btn btn-success orderBtn">Place Order <i class="glyphicon glyphicon-menu-right"></i></a>-->
<!--    </div>-->
<!--</div>-->
<!--</body>-->
<!--</html>-->


<!--NUOVO CHECKOUT-->
<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--    <meta name="description" content="">-->
<!--    <meta name="author" content="">-->
<!--    <link rel="icon" href="../../../../favicon.ico">-->

    <title>RobinFood-Checkout</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
    <style>
    body {
      /* background-color: silver; */
    }
    h2 {
      margin-top: 50px;
      margin-bottom: 40px;
      text-align: center;
      font-family: Quicksand;
      font-style: oblique;
      text-transform: uppercase;
      font-size: 40px;
      font-weight: bold;
    }
    </style>
</head>

<body class="bg-light">

<div class="container">
    <div>
<!--        <img class="d-block mx-auto mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">-->
        <h2>Checkout</h2>
<!--        <p class="lead">Below is an example form built entirely with Bootstrap's form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>-->
    </div>


    <form class="needs-validation" id="checkout-form" action="cartAction.php" method="post" novalidate>
    <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
            <div class="sticky-top">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Il tuo carrello</span>
                    <span class="badge badge-secondary badge-pill"><?php echo $cart->total_items(); ?></span>
                </h4>
                <ul class="list-group mb-3">
                    <?php
                    if($cart->total_items() > 0){
                        //get cart items from session
                        $cartItems = $cart->contents();
                        foreach($cartItems as $item){
                            ?>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0"><?php echo $item["name"]; ?></h6>
                                    <small class="text-muted"><?php echo $fmt->formatCurrency($item['price'], 'EUR')." x ". $item['qty'] ?></small>
                                </div>
                                <span class="text-muted"><?php echo $fmt->formatCurrency($item["subtotal"], "EUR"); ?></span>
                            </li>
                        <?php } } else { ?>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            Il tuo carrello è vuoto...
                        </li>
                    <?php } ?>
                    <li class="list-group-item d-flex justify-content-between">
                        <span>Totale (EURO)</span>
                        <strong><?php echo $fmt->formatCurrency($cart->total(), "EUR"); ?></strong>
                    </li>
                </ul>
                <div class="card p-2">
                        <div class="input-group">
                            <h6><label for="deliveryHour">Orario di consegna (Orario di apertura: 12:00 - 22:00)</label></h6>
                            <input type="time" class="form-control w-100" name="deliveryHour" id="deliveryHour" min="12:00" max="22:00" required>
                            <div class="invalid-feedback">
                                Inserire un orario valido.
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Indirizzo di spedizione</h4>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Nome</label>
                        <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="<?=$custRow['user_nome']?>" disabled>
<!--                        <div class="invalid-feedback">-->
<!--                            Valid first name is required.-->
<!--                        </div>-->
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Cognome</label>
                        <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="<?=$custRow['user_cognome']?>" disabled>
                        <div class="invalid-feedback">
                            E' obbligatorio inserire un cognome valido.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="username">Username</label>
                    <div class="input-group">
<!--                        <div class="input-group-prepend">-->
<!--                            <span class="input-group-text">@</span>-->
<!--                        </div>-->
                        <input type="text" class="form-control" name="username" id="username" placeholder="" value="<?=$custRow['user_uid']?>" disabled>
                        <div class="invalid-feedback" style="width: 100%;">
                            E' obbligatorio inserire uno username.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="" value="<?=$custRow['user_email']?>" disabled>
<!--                    <div class="invalid-feedback">-->
<!--                        Please enter a valid email address for shipping updates.-->
<!--                    </div>-->
                </div>

                <div class="mb-3">
                    <label for="phone">Numero di telefono</label>
                    <input type="tel" class="form-control" name="phone" id="phone" placeholder="" value="<?=$custRow['user_phone']?>" disabled>
                </div>

<!--                <div class="mb-3">-->
<!--                    <label for="address">Address</label>-->
<!--                    <input type="text" class="form-control" id="address" placeholder="1234 Main St" required>-->
<!--                    <div class="invalid-feedback">-->
<!--                        Please enter your shipping address.-->
<!--                    </div>-->
<!--                </div>-->
                <div class="row">
                    <div class="col-md-9 mb-3">
                        <label for="route">Indirizzo</label>
<!--                        <span id="locbtns">-->
                        <button type="button" id="locbtn" class="btn btn-primary btn-sm ml-3 my-btn">Da posizione attuale</button>
                        <button type="button" id="predefbtn" class="btn btn-primary btn-sm ml-3 my-btn"
                        <?php
                            if (is_null($custRow['user_address'])) {
                                echo "disabled";
                            }
                        ?>>Memorizzato</button>
<!--                        </span>-->
                        <input type="text" class="form-control" name="route" id="route" placeholder="Via ..." onfocus="geolocate()" required>
                        <div class="invalid-feedback">
                            Inserisci il tuo indirizzo di consegna.
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="street_number">N. civico</label>
                        <input type="text" class="form-control" name="street_number" id="street_number" placeholder="123" required>
                        <div class="invalid-feedback">
                            Inseriesci il tuo numero civico.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="address2">Indirizzo 2 <span class="text-muted">(Opzionale)</span></label>
                    <input type="text" class="form-control" name="address2" id="address2" placeholder="Interno">
                </div>

                <div class="row">
                    <div class="col-md-5 mb-3">
                        <label for="locality">Città</label>
                        <input type="text" class="form-control" name="locality" id="locality" placeholder="Forlì" required>
                        <div class="invalid-feedback">
                            Digita una località valida.
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="administrative_area_level_2">Provincia</label>
                        <select class="custom-select d-block w-100" name="administrative_area_level_2" id="administrative_area_level_2" required>
                            <option value="">Scegli...</option>
                            <option value="FC">Forlì-Cesena</option>
                        </select>
                        <div class="invalid-feedback">
                            Inserisci uno stato valido.
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="postal_code">CAP</label>
                        <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="" required>
                        <div class="invalid-feedback">
                            CAP obbligatorio.
                        </div>
                    </div>
                </div>
<!--                <hr class="mb-4">-->
<!--                <div class="custom-control custom-checkbox">-->
<!--                    <input type="checkbox" class="custom-control-input" id="same-address">-->
<!--                    <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>-->
<!--                </div>-->
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="save-info" id="save-info" value="save">
                    <label class="custom-control-label" for="save-info">Salva queste informazioni per il futuro</label>
                </div>
                <hr class="mb-4">

                <h4 class="mb-3">Pagamento</h4>

                <div class="d-block my-3">
                    <div class="custom-control custom-radio">
                        <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" value="credit_card" checked required>
                        <label class="custom-control-label" for="credit">Carta di credito</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" value="debit_card" required>
                        <label class="custom-control-label" for="debit">Carta debit</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" value="paypal" required>
                        <label class="custom-control-label" for="paypal">Paypal</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="cc-name">Nome della carta</label>
                        <input type="text" class="form-control" name="cc-name" id="cc-name" placeholder="" required>
                        <small class="text-muted">Nome completo come riportato sulla carta</small>
                        <div class="invalid-feedback">
                            Il nome della carta è obbligatorio.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="cc-number">Numero carta di credito</label>
                        <input type="text" class="form-control" name="cc-number" id="cc-number" placeholder="" required>
                        <div class="invalid-feedback">
                            Il numero della carta è obbligatorio.
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 mb-3">
                        <label for="cc-expiration">Data di scadenza</label>
                        <input type="text" class="form-control" name="cc-expiration" id="cc-expiration" placeholder="" required>
                        <div class="invalid-feedback">
                            La data di scadenza è obbligatoria.
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <label for="cc-expiration">CVV</label>
                        <input type="text" class="form-control" name="cc-cvv" id="cc-cvv" placeholder="" required>
                        <div class="invalid-feedback">
                            Il codice di sicurezza è obbligatorio.
                        </div>
                    </div>
                </div>
                <hr class="mb-4">
                <input type="hidden" name="action" id="action" value="placeOrder">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Continua al checkout</button>
<!--                <button class="btn btn-primary btn-lg btn-block" type="button" id="submitBtn" onclick="this.form.submit()">Continue to checkout</button>-->
<!--                <a href="cartAction.php?action=placeOrder" class="btn btn-primary btn-lg btn-block">Place Order</a>-->
                <a href="homepage.php" class="btn btn-warning btn-lg btn-block">Continua lo shopping</a>

        </div>

    </div>
    </form>
    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2018 RobinFood</p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Privacy</a></li>
            <li class="list-inline-item"><a href="#">Terms</a></li>
            <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
    </footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
<!--<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>-->
<!--<script src="../../../../assets/js/vendor/popper.min.js"></script>-->
<!--<script src="../../../../dist/js/bootstrap.min.js"></script>-->
<!--<script src="../../../../assets/js/vendor/holder.min.js"></script>-->
<script src="geolocation.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA5QerMC6dnwStxWppbRam_s1PPudJ2o0&libraries=places&callback=initAutocomplete"
        async defer></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#predefbtn').click(function () {
            // console.log("prefefbtn clicked!");
            $.getJSON("checkoutAction.php", {checkout_action:"getStoredUserAddress"}, function (data) {
                console.log(data);
                if (data.status == 'ok') {
                    $.each(JSON.parse(data.address), function (key, value) {
                        document.getElementById(key).value = value;
                    })
                } else {
                    alert("Cannot retrieve user\'s address");
                }
            });
        });
        $('#checkout-form').keypress(function(e) {
            var $targ = $(e.target);
            if (!$targ.is(":button, :submit") && (e.keyCode ? e.keyCode : e.which) == 13) {
                e.preventDefault();
            }
        });
        $(':checkbox').keypress(function(e){
            if((e.keyCode ? e.keyCode : e.which) == 13){
                $(this).trigger('click');
            }
        });

    });
</script>

<script>

    // function addLeadingZero(val) {
    //     if (val < 10) {
    //         return "0" + val;
    //     }
    //     return val;
    // }
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';

        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    // if (this.id === 'checkout-form') {
                    //     var cuurentDate = new Date();
                    //     var minDate = new Date(currentDate.getTime() + (20*60*1000));
                    //     document.getElementById('deliveryHour').setAttribute('min', addLeadingZero(minDate.getHours()) + ":" + addLeadingZero(minDate.getMinutes()));
                    // }
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
</body>
</html>
