
<div class="carousel-caption d-none d-md-block animated bounceInDown">
  <h1>RobinFood</h1>
  <h4>cibi ricchi a prezzi poveri!</h4>
</div>
<div id="homepage" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#homepage" data-slide-to="0" class="active"></li>
    <li data-target="#homepage" data-slide-to="1"></li>
    <li data-target="#homepage" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" style="background:silver;">
    <div class="carousel-item active" style="background-image: url(images/pizza.jpg); opacity:0.6">
    </div>
    <div class="carousel-item" style="background-image: url(images/hb.jpg); opacity:0.6">
    </div>
    <div class="carousel-item" style="background-image: url(images/cheesecake.jpg); opacity:0.6">
    </div>
  </div>
  <a class="carousel-control-prev" href="#homepage" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#homepage" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php

function debug_to_console($data) {
    if (is_array($data) || is_object($data)) {
        echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
    } else {
        echo("<script>console.log('PHP: ".$data."');</script>");
	}
}

 function checkLogin() {
     return isset($_SESSION['u_id']) ? '<a href="#0" class="cd-add-to-cart" data-price="" title="aggiungi al carrello">+<i class="fa fa-shopping-cart"></i></a>' : "";
}
 ?>

<script>
function deleteItem(id) {
    if (confirm("Sei sicuro di voler eliminare il prodotto?")) {
        $.post("includes/product-inc.php", {action: "deleteItem", id: id}, function (data) {
            if (data == 'ok') {
                location.reload();
            } else {
                alert('Eliminazione del prodotto fallita, perfavore riprova.');
            }
        });
    }
}
</script>
<!-- The Modal -->
<div id="myModal" class="mymodal">
  <span class="myclose">&times;</span>
  <img class="modal-content" id="img" alt="immagine del prodotto selezionato">
  <div id="caption"></div>
</div>

<div data-spy="scroll" id="navbar-scroll" data-offset="0">
<?php
    include_once 'includes/dbh-inc.php';
    $fmt = new NumberFormatter('it_IT', NumberFormatter::CURRENCY);
    $result_cat = $conn->query("SELECT * FROM categories ORDER BY category_id ASC");
    if ($result_cat->num_rows > 0) {
        while ($row_cat = $result_cat->fetch_assoc()) {
            debug_to_console("Next category");
            debug_to_console($row_cat);
            echo "<section class='".strtolower($row_cat["name"])."' id='".strtolower($row_cat["name"])."'>";
            echo "<h1 id='".strtolower($row_cat["name"])."'>".ucfirst($row_cat["name"])."</h1>";
            if (isset($_SESSION['u_id']) && $_SESSION['u_type'] == "admin") {
                echo "<button type='button' class='btn btn-info btn-add-product' data-toggle='modal' data-target='#a'>+</button>";
            }
            $result_prod = $conn->query("SELECT * FROM products WHERE category_id = {$row_cat['category_id']}");
            if ($result_prod->num_rows > 0) {
                echo "<div class='container-fluid menu'>";
                while ($row_prod = $result_prod->fetch_assoc()) {
                    debug_to_console($row_prod);
                    echo "<div class='row align-items-center'>";
                        echo "<div class='col-2 col-md-3'>";
                            if (!is_null($row_prod['image'])) {
                                echo "<img class='img-fluid'src='images/{$row_prod['image']}' alt='{$row_prod['name']}'/>";
                            }
                            echo "</div>";
                        echo "<div class='col-7 col-md-6'>";
                            echo $row_prod['name'];
                        echo "</div>";
                        echo "<div class='col-2 col-md-2 prezzo'>";
                            echo $fmt->formatCurrency($row_prod['price'], "EUR");
                        echo "</div>";

                        echo "<div class='col-1'>";
                        if (isset($_SESSION['u_id']) && $_SESSION['u_type'] == "admin") {
                          echo "<button type='button' class='btn btn-danger btn-delete-product' onclick='deleteItem({$row_prod['product_id']})'>-</button>";
                        }
                            if (isset($_SESSION['u_id']) && $_SESSION['u_type'] == "customer") {
                                echo "<a href='cartAction.php?action=addToCart&id={$row_prod['product_id']}'><i class='fa fa-shopping-cart'></i></a>";
                            }
                        echo "</div>";
                        echo "</div>"; //close row
                        echo "<hr />";

                }
                echo "</div>";  //close menu
            } else {
                echo "<p>No products found for this category.</p>";
            }
            echo "</section>";
        }
    } else {
        echo "<p> No categories found.</p>";
    }
?>
</div>

<div class="modal fade" id="a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-add-product" id="b">
      <div class="modal-header">
        <h5 class="modal-title" id="c">Aggiungi prodotto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="was-validated products-form" id="addProductForm" action="includes/product-inc.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <input type="text" name="nome" class="form-control is-valid" id="validationServer01" placeholder="Nome prodotto" required />
          </div>
          <div class="form-group">
            <input type="text" name="descrizione" class="form-control is-valid" id="validationServer02" placeholder="Descrizione prodotto" required />
          </div>
          <div class="form-group">
            <select class="custom-select" id="select-category" name="categoria" required>
              <option value="">Scegli la categoria</option>
              <?php
                include_once "includes/dbh-inc.php";
                $result=$conn->query("SELECT * FROM categories");
                if($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {
                    echo "<option value = '{$row['category_id']}'>{$row['name']}</option>";
                  }
                }
              ?>
            </select>
          </div>
          <div class="form-group">
            <input type="number" class="form-control is-valid" step="0.01" min="0.01" name="prezzo" placeholder="Prezzo" required />
          </div>
          <div class="custom-file" id="div-img">
            <input type="file" name="fileToUpload" class="custom-file-input" accept="image/*" id="fileToUpload" required>
            <label class="custom-file-label" for="validatedCustomFile">Scegli immagine</label>
          </div>
            <input type="hidden" value="submit" name="submit" id ="addProductSubmit">
            <button type="submit" class="btn btn-primary" id="addProduct" style="margin-top:15px;">Aggiungi prodotto</button>
        </form>
      </div>
    </div>
  </div>
</div>
		<section class="about-content" id="about">
			<h1>About</h1>
			<p>Il team di RobinFood garantisce qualità dei prodotti,
          affidabilità e velocità nella consegna.<br />
          La nostra area di spedizione si estende su tutta la provincia di Forlì-Cesena senza costi aggiuntivi!<br />
          La nostra specialità? La qualità e il prezzo!<br />
          RobinFood, cibi ricchi a prezzi poveri!
      </p>
		</section>
	<div class="space" id="foot">
	</div>
	</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
if( !window.jQuery ) document.write('<script src="js/jquery-3.0.0.min.js"><\/script>');

$(".menu img").click(function () {
    $("#img").attr("src", $(this).attr("src"));
    $("#caption").html($(this).attr("alt"));
    $("#myModal").show();
});

$(".myclose").click(function () {
    $("#myModal").hide();
})
</script>

<script>
    $(document).ready(function () {
        $("#addProduct").click(function (event) {
            event.preventDefault();
            var form = $("#addProductForm")[0];
            if (form.checkValidity()) {
                var data = new FormData(form);
                // console.log(data);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: 'includes/product-inc.php',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        // console.log("Data: " + data);
                        alert(data);
                        location.reload();
                    }
                });
            }
        });
        $("#select-category").on('change', function () {
          if ($(this).val() === "5") {
            $("#div-img").hide();
            $("#fileToUpload").prop("required", false);
            // $("#fileToUpload").prop("disabled", true);

          }
          else {
            $("#div-img").show();
            // $("#fileToUpload").prop("disabled", false);
            $("#fileToUpload").prop("required", true);
          }
        });
    });
</script>
