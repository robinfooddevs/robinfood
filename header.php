<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="it">
<head>
	<title>RobinFood</title>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Mr+Bedfort" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Ruthie" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Quicksand:500" rel="stylesheet" />

	<link type="text/css" rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="footer.css" />
	<link type="text/css" rel="stylesheet" href="homepage.css" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="navbar.js"></script>
	<script src="signup.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .count {
            position: relative;
            top: -20px !important;
            left: -15px;
        }
        .btn-notifiche::after {
            /*position: absolute;*/
            /*margin-left: -20px !important;*/
        }

        .new-notification {
            background-color: #00aced;
        }
        .notification-item {
            margin-bottom: 0;
        }
        .notification-item:hover {
            background-color: white;
            color: black;
        }
    </style>
</head>
<body data-spy="scroll" data-target="#navbar-scroll">
	<nav id="navbar-scroll" class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  	<a class="navbar-brand" href="#">RobinFood</a>

		<?php if(isset($_SESSION['u_id'])): ?>
			<span id="greetings">Ciao <?=$_SESSION['u_uid']?></span>
			<a href="viewCart.php" id="go_to_cart">
				<i class="fa fa-shopping-cart fa-navbar fa-2x"></i>
			</a>
			<div class="dropdown dropdown-notifiche">
				<button class="btn btn-secondary dropdown-toggle btn-notifiche" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="Notifiche">
<!--					<span id="testo-notifiche">Notifiche</span>-->
					<i class="fa fa-bell-o fa-2x"></i>
                    <span class="badge badge-pill badge-danger count" style="border-radius:10px;"></span>
				</button>
				<ul class="dropdown-menu  dropdown-menu-right ul-notifications" aria-labelledby="dropdownMenuButton">
				</ul>
			</div>
			<button class="navbar-toggler navbar-toggler-center" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  	<span class="navbar-toggler-icon"></span>
		  </button>
		<?php else: ?>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  	</button>
		<?php endif; ?>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
    	<ul class="navbar-nav mr-auto">
      	<li class="nav-item" id="link-home">
        	<a class="nav-link" href="#homepage">Home</a>
      	</li>
				<li class="nav-item dropdown">
        	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          	I nostri piatti
        	</a>
        	<div class="dropdown-menu" aria-labelledby="navbarDropdown">
          	<a class="dropdown-item" href="#pizze">Pizze</a>
						<a class="dropdown-item" href="#primi">Primi</a>
			    	<a class="dropdown-item" href="#panini">Panini</a>
						<a class="dropdown-item" href="#dolci">Dolci</a>
          	<a class="dropdown-item" href="#bevande">Bevande</a>
        	</div>
      	</li>
      	<li class="nav-item">
        	<a class="nav-link" href="#about">Chi siamo</a>
      	</li>
      	<li class="nav-item">
        	<a class="nav-link" href="#foot">Contattaci</a>
      	</li>
				<?php
				if (isset($_SESSION['u_id']) && $_SESSION['u_type'] == "admin") {
					echo "<button class='btn btn-outline-success my-2 my-sm-0' name='submit'><a style='text-decoration:none;color:white;' href='viewOrders.php'>Ordini</a></button>";
				}
				?>
    	</ul>
      <?php if(!isset($_SESSION['u_id'])): ?>
				<form action="includes/login-inc.php" class="form-inline my-2 my-lg-0" method="post">
    			<input class="form-control mr-sm-2" id="form-control-user" type="text" name="uid" placeholder="Username">
					<input class="form-control mr-sm-2" id="pass-control-user" type="password" name="psw" placeholder="Password">
    			<button class="btn btn-login btn-outline-success my-2 my-sm-0" type="submit" name="submit">Login</button>
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary btn-submit" data-toggle="modal" data-target="#exampleModalCenter">
						Registrati
					</button>
  			</form>
      <?php else: ?>
				<form action="includes/logout-inc.php" class="form-inline my-2 my-lg-0" method="post">
          <button class="btn btn-logout btn-outline-warning my-2 my-sm-0" id="notification" type="submit" name="submit">
          	<span class="glyphicon glyphicon-log-out"></span> Logout
          </button>
        </form>
      <?php endif; ?>
		</div>
	</nav>
	<!-- Modal -->
	<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content" id="modal-submit">
      	<div class="modal-header">
        	<h5 class="modal-title" id="exampleModalLongTitle">Registrazione</h5>
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          	<span aria-hidden="true">&times;</span>
        	</button>
      	</div>
      	<div class="modal-body">
        	<form action="includes/signup-inc.php" class="login-form" method="post">
						<div class="form-group" onSubmit="return Validate()" name="vform">
							<label for="exampleInputNome1"></label>
							<input type="text" class="form-control" id="exampleInputNome1" name="nome" oninput="check_nome(this)" placeholder="Nome" required />
							<script language='javascript' type='text/javascript'>
							function check_nome(input) {
								var letter = /^[a-zA-Z]+$/;
								if (!input.value.match(letter)) {
									input.setCustomValidity('Il nome deve contenere solo lettere.');
								} else {
								input.setCustomValidity('');
								}
							}
							</script>
							<label for="exampleInputCognome1"></label>
							<input type="text" class="form-control" id="exampleInputCognome1" name="cognome" placeholder="Cognome" oninput="check_cognome(this)" required />
							<script language='javascript' type='text/javascript'>
    						function check_cognome(input) {
									var letter = /^[a-zA-Z]+$/;
        					if (!input.value.match(letter)) {
            				input.setCustomValidity('Il cognome deve contenere solo lettere.');
        					} else {
            			input.setCustomValidity('');
        					}
    						}
							</script>
							<label for="exampleInputEmail1"></label>
							<input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" required />
							<label for="exampleInputUsername1"></label>
							<input type="tel" class="form-control" id="exampleInputPhone1" name="phone" minlength="9" maxlength="10" oninput="check_tel(this)" placeholder="Telefono" required />
							<script language='javascript' type='text/javascript'>
							function check_tel(input) {
								var number = /^[0-9]+$/;
								if (!input.value.match(number) ) {
									input.setCustomValidity('Il telefono deve contenere solo numeri.');
								} else {
								input.setCustomValidity('');
								}
							}
							</script>
							<label for="exampleInputTelefono1"></label>
							<input type="text" class="form-control" id="exampleInputUsername1"  name="uid" placeholder="Username" required />
							<label for="exampleInputPassword1"></label>
							<input type="password" class="form-control" id="pass" name="psw" placeholder="Password" required />
							<label for="exampleInputRepeatPassword1"></label>
							<input type="password" class="form-control" id="confirmpass" data-container="body" data-toggle="popover" data-placement="right" oninput="check(this)" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." placeholder="Conferma Password" required />
							<script language='javascript' type='text/javascript'>
    						function check(input) {
        					if (input.value != document.getElementById('pass').value) {
            				input.setCustomValidity('Le due password devono coincidere!');
        					} else {
            			input.setCustomValidity('');
        					}
    						}
							</script>

						</div>
						<button type="submit" class="btn btn-primary" id="submit-btn" name="submit">Submit</button>
					</form>
					<div id="pswd_info">
		        	<h1>Sicurezza per la password</h1>
		        	<ul>
		            <li id="letter" class="invalid">Almeno <strong>una lettera</strong></li>
		            <li id="capital" class="invalid">Almeno <strong>una lettera maiuscola</strong></li>
		            <li id="number" class="invalid">Almeno <strong>un numero</strong></li>
		            <li id="length" class="invalid">Deve essere di almeno <strong>8 caratteri</strong></li>
		            <li id="space" class="invalid">Devi <strong> usare [~,!,@,#,$,%,^,&,*,-,=,.,;,']</strong></li>
		        </ul>
		    	</div>
      	</div>
    	</div>
  	</div>
	</div>


	<script>

		// $('body').scrollspy({ target: '#navbar-scroll' });
		$(".navbar-collapse ul li a[href^='#']").on('click',function(e){
			target = this.hash;
			e.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 600, function(){
				window.location.hash = target;
			});
		});

		$(".btn-login").on('click', function() {
			if ($("#form-control-user").val() == "" || $("#pass-control-user").val() == "") {
				alert('Username o password mancanti!');
			}
		});


	</script>

    <?php if(isset($_SESSION['u_id'])): ?>
    <script>
        $(document).ready(function () {
            function load_unseen_notification(view ='') {
                $.post("fetch.php", {view:view}, function (data) {
                    if (view !== '') {
                        $('.ul-notifications').html(data.notification);
                    }
                    if (data.unseen_notification > 0) {
                        $('.count').html(data.unseen_notification);
                    }
                }, "json");
            }
            load_unseen_notification();
            $(".btn-notifiche").click(function () {
                $('.count').html('');
                load_unseen_notification('yes');
            });
						$("#submit-btn").on('click', function(e) {
							e.preventDefault();
						})

            setInterval(function () {
                load_unseen_notification();
            }, 5000);
        });
    </script>
    <?php endif; ?>
